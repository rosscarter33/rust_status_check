use std::{env, fs, io, path};

struct SimDir {
    path: path::PathBuf,
}

impl SimDir {
    fn print(&self) -> () {
        println!("{}", self.path.display())
    }
}

fn main() -> Result<(), io::Error> {
    let args: Vec<String> = env::args().collect();
    let autodata_path = path::Path::new(&args[1]);

    for entry in fs::read_dir(autodata_path)? {
        let path = entry?.path();
        if path.is_dir() {
            SimDir { path: path }.print();
        }
    }

    Ok(())
}
